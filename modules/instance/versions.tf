terraform {
  required_providers {
    ct = {
      source  = "poseidon/ct"
      version = "0.9.2"
    }
  }

  required_version = ">= 0.15.3"
}
